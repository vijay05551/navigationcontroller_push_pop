//
//  ViewController2.m
//  working with navigation
//
//  Created by osx on 03/08/16.
//  Copyright © 2016 Ameba Technologies. All rights reserved.
//

#import "ViewController2.h"
#import "ViewController5.h"

@interface ViewController2 ()

@end

@implementation ViewController2

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)backBtnAction:(id)sender
{
    ViewController5 *view = [self.storyboard instantiateViewControllerWithIdentifier:@"viewController5"];
    [self.navigationController pushViewController:view animated:YES];
}


@end
