//
//  ViewController5.m
//  working with navigation
//
//  Created by osx on 03/08/16.
//  Copyright © 2016 Ameba Technologies. All rights reserved.
//

#import "ViewController5.h"

@interface ViewController5 ()

@end

@implementation ViewController5

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [[self navigationController] setNavigationBarHidden:YES];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    
}

- (IBAction)backBtnAction:(id)sender
{
    NSMutableArray *views = [NSMutableArray arrayWithArray:self.navigationController.viewControllers];
    NSLog(@"%@",views);
    NSLog(@"%lu",(unsigned long)views.count);
    
    [views removeObjectAtIndex:[views count]-1];
    
    self.navigationController.viewControllers = views;
    [self.navigationController popViewControllerAnimated:YES];
}


@end
